// (Lines like the one below ignore selected Clippy rules
//  - it's useful when you want to check your code with `cargo make verify`
// but some rules are too "annoying" or are not applicable for your case.)
#![allow(clippy::wildcard_imports)]

use std::collections::VecDeque;

use seed::{prelude::*, *};

// ------ ------
//     Init
// ------ ------

// `init` describes what should happen when your app started.
fn init(_: Url, orders: &mut impl Orders<Msg>) -> Model {
    // log!("Init");

    orders
        .stream(streams::window_event(Ev::KeyPress, |event| {
            if let Ok(event) = event.dyn_into::<web_sys::KeyboardEvent>() {
                // event.prevent_default();
                match event.key().to_lowercase().as_str() {
                    "w" => Some(Msg::KeyPressed(Key::W)),
                    "a" => Some(Msg::KeyPressed(Key::A)),
                    "s" => Some(Msg::KeyPressed(Key::S)),
                    "d" => Some(Msg::KeyPressed(Key::D)),
                    _ => None,
                }
            } else {
                None
            }
        }))
        .stream(streams::window_event(Ev::TouchStart, |event| {
            if let Ok(event) = event.dyn_into::<web_sys::TouchEvent>() {
                event.prevent_default();
                let touches = event.touches();
                if touches.length() != 1 {
                    Msg::Log(format!(
                        "Touch started, but more than a single one is in progress."
                    ))
                } else {
                    match touches.item(0) {
                        Some(touch) => {
                            let x = touch.screen_x();
                            let y = touch.screen_y();
                            Msg::TouchStarted(x, y)
                        }
                        None => Msg::Log(format!("Touch started, but there are no touches. WTF?")),
                    }
                }
            } else {
                Msg::Log(format!("Failed to cast the event into a TouchStart."))
            }
        }))
        .stream(streams::window_event(Ev::TouchMove, |event| {
            if let Ok(event) = event.dyn_into::<web_sys::TouchEvent>() {
                // This is important for iOS browsers to avoid weird scrolling and bouncing.
                // FIXME: This won't do anything unless event listener is set to static. See index.html for a workaround.
                event.prevent_default();
                let touches = event.touches();
                if touches.length() != 1 {
                    Msg::Log(format!(
                        "Touch moved, but more than a single one is in progress."
                    ))
                } else {
                    match touches.item(0) {
                        Some(touch) => {
                            let x = touch.screen_x();
                            let y = touch.screen_y();
                            Msg::TouchMoved(x, y)
                        }
                        None => Msg::Log(format!("Touch moved, but there are no touches. WTF?")),
                    }
                }
            } else {
                Msg::Log(format!("Failed to cast the event into a TouchMove."))
            }
        }))
        .stream(streams::window_event(Ev::TouchEnd, |event| {
            if let Ok(event) = event.dyn_into::<web_sys::TouchEvent>() {
                event.prevent_default();
                let count = event.touches().length();
                if count == 0 {
                    Msg::TouchEnded
                } else {
                    Msg::Log(format!(
                        "Touch ended, but more than a single one {count} is in progress."
                    ))
                }
            } else {
                Msg::Log(format!("Failed to cast the event into a TouchEnd."))
            }
        }))
        .stream(streams::window_event(Ev::TouchCancel, |_| {
            Msg::TouchCancelled
        }))
        .stream(streams::interval(250, || Msg::ClockTick));

    Model {
        logs: VecDeque::new(),
        swipe: None,
        snake: Snake::new(Direction::East, 16),
    }
}

// ------ ------
//     Model
// ------ ------

// `Model` describes our app state.
#[derive(Debug)]
struct Model {
    logs: VecDeque<String>,
    swipe: Option<Swipe>,
    snake: Snake,
}

#[derive(Debug)]
struct Snake {
    direction: Direction,
    head: Point,
    segments: VecDeque<Direction>,
}

impl Snake {
    fn advance(&mut self) -> &Self {
        self.head.shift(self.direction);
        self.segments.push_front(self.direction.clone());
        self.segments.pop_back();
        self
    }

    fn grow(&mut self, direction: Direction) -> &Self {
        self.segments.push_front(direction.clone());
        self
    }

    fn new(direction: Direction, lenght: u8) -> Snake {
        let mut snake = Snake {
            direction,
            head: Point::default(),
            segments: VecDeque::default(),
        };
        let mut i = 0;
        while i < lenght {
            snake.grow(Direction::West);
            i += 1
        }
        snake
    }
}

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq)]
struct Point {
    x: i32,
    y: i32,
}

impl Point {
    fn new(x: i32, y: i32) -> Self {
        Point { x, y }
    }
    fn shift(&mut self, direction: Direction) -> &Self {
        match direction {
            Direction::North => self.y -= 1,
            Direction::East => self.x += 1,
            Direction::South => self.y += 1,
            Direction::West => self.x -= 1,
        };
        self
    }

    fn trace<'a, C>(&self, directions: C) -> Vec<Point>
    where
        C: Iterator<Item = &'a Direction>,
    {
        let mut points = Vec::default();
        let mut first = self.clone();

        directions.into_iter().for_each(|direction| {
            first.shift(direction.opposite());
            points.push(first.clone())
        });
        points
    }
}

#[cfg(test)]
mod point_tests {

    use super::*;

    #[test]
    fn trace_vec() {
        let directions = vec![Direction::North, Direction::East, Direction::East];
        let start = Point { x: 3, y: -2 };
        let expected = vec![
            Point { x: 3, y: -1 },
            Point { x: 2, y: -1 },
            Point { x: 1, y: -1 },
        ];
        assert_eq!(expected, start.trace(directions.iter()))
    }

    #[test]
    fn trace_vec_deque() {
        let directions: VecDeque<Direction> =
            vec![Direction::South, Direction::West, Direction::North].into();
        let start = Point::new(-5, 2);
        let expected = vec![
            Point { x: -5, y: 1 },
            Point { x: -4, y: 1 },
            Point { x: -4, y: 2 },
        ];
        assert_eq!(expected, start.trace(directions.iter()))
    }
}

#[derive(Debug)]
struct Swipe {
    from_x: i32,
    from_y: i32,
    to_x: i32,
    to_y: i32,
}

impl Swipe {
    fn direction(&self, threshold: i32) -> Option<Direction> {
        let delta_x = self.to_x - self.from_x;
        let delta_y = self.to_y - self.from_y;

        if delta_x.abs() > delta_y.abs() {
            if delta_x > threshold {
                Some(Direction::East)
            } else if delta_x < -threshold {
                Some(Direction::West)
            } else {
                None
            }
        } else {
            if delta_y > threshold {
                Some(Direction::South)
            } else if delta_y < -threshold {
                Some(Direction::North)
            } else {
                None
            }
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Direction {
    North,
    East,
    South,
    West,
}

impl Direction {
    fn opposite(&self) -> Direction {
        match self {
            Direction::North => Direction::South,
            Direction::East => Direction::West,
            Direction::South => Direction::North,
            Direction::West => Direction::East,
        }
    }
}

#[cfg(test)]
mod direction_tests {
    use super::*;

    #[test]
    fn opposite_test() {
        assert_eq!(Direction::North, Direction::South.opposite());
        assert_eq!(Direction::East, Direction::West.opposite());
        assert_eq!(Direction::South, Direction::North.opposite());
        assert_eq!(Direction::West, Direction::East.opposite());
    }
}

// ------ ------
//    Update
// ------ ------

// (Remove the line below once any of your `Msg` variants doesn't implement `Copy`.)
#[derive(Clone, Debug)]
// `Msg` describes the different events you can modify state with.
enum Msg {
    Log(String),
    ClockTick,
    KeyPressed(Key),
    TouchStarted(i32, i32),
    TouchMoved(i32, i32),
    TouchEnded,
    TouchCancelled,
}
#[derive(Copy, Clone, Debug)]
enum Key {
    W,
    A,
    S,
    D,
}

// `update` describes how to handle each `Msg`.
fn update(msg: Msg, model: &mut Model, orders: &mut impl Orders<Msg>) {
    // log!("Update", msg);

    match msg {
        Msg::Log(message) => model.logs.push_front(message),
        Msg::KeyPressed(Key::W) => {
            model.snake.direction = Direction::North;
        }
        Msg::KeyPressed(Key::A) => {
            model.snake.direction = Direction::West;
        }
        Msg::KeyPressed(Key::S) => {
            model.snake.direction = Direction::South;
        }
        Msg::KeyPressed(Key::D) => {
            model.snake.direction = Direction::East;
        }
        Msg::TouchStarted(x, y) => {
            model.swipe = Some(Swipe {
                from_x: x,
                from_y: y,
                to_x: x,
                to_y: y,
            });
            ()
        }
        Msg::TouchMoved(x, y) => {
            if let Some(ref mut swipe) = model.swipe {
                swipe.to_x = x;
                swipe.to_y = y;
            };
        }
        Msg::TouchEnded => model.swipe = None,

        Msg::TouchCancelled => model.swipe = None,

        Msg::ClockTick => {
            if let Some(ref mut swipe) = model.swipe {
                orders.send_msg(Msg::Log(format!(
                    "Swiping from {}, {} to {}, {} ({}, {})",
                    swipe.from_x,
                    swipe.from_y,
                    swipe.to_x,
                    swipe.to_y,
                    swipe.to_x - swipe.from_x,
                    swipe.to_y - swipe.from_y,
                )));
                if let Some(direction) = swipe.direction(2) {
                    orders.send_msg(Msg::Log(format!("Swiping {:#?}", direction)));
                    model.snake.direction = direction;
                };
                swipe.from_x = swipe.to_x;
                swipe.from_y = swipe.to_y;
            };
            model.snake.advance();
        }
    }
}

// ------ ------
//     View
// ------ ------

// `view` describes what to display.
fn view(model: &Model) -> Vec<Node<Msg>> {
    vec![
        div![
            style! {
                St::Padding => px(20),
            },
            h1!["Save the snake, dammit!"],
            p!["Use W A S D to move around."],
        ],
        svg![
            style! {
                St::Height => percent(100),
                St::Background => "salmon",
            },
            attrs! {
                At::ViewBox => "-25 -25 50 50",
            },
            text![
                attrs! {
                    At::Transform => format!("translate({}, {})", model.snake.head.x, model.snake.head.y)
                },
                style![
                    St::FontSize => px(3),
                    St::from("text-anchor") => "middle",
                    St::from("dominant-baseline") => "middle",
                ],
                "o"
            ],
            model
                .snake
                .head
                .trace(model.snake.segments.iter())
                .iter()
                .map(|point| {
                    text![
                        attrs! {
                            At::Transform => format!("translate({}, {})", point.x, point.y)
                        },
                        style![
                            St::FontSize => px(2),
                            St::from("text-anchor") => "middle",
                            St::from("dominant-baseline") => "middle",
                        ],
                        "o"
                    ]
                }),
        ],
        // pre![if let Some(swipe) = &model.swipe {
        //     format!("{:#?}", swipe)
        // } else {
        //     "No swipe".to_string()
        // }],
        // pre![
        //     style! {
        //         St::Height => px(100),
        //         St::Overflow => "scroll",
        //     },
        //     format!("{:#?}", &model.logs)
        // ],
    ]
}

// ------ ------
//     Start
// ------ ------

// (This function is invoked by `init` function in `index.html`.)
#[wasm_bindgen(start)]
pub fn start() {
    // Mount the `app` to the element with the `id` "app".
    App::start("app", init, update, view);
}
